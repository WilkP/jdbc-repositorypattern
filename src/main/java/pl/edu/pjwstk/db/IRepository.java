package pl.edu.pjwstk.db;

/**
 * Created by vpnk on 11.11.16.
 */
public interface IRepository<TEntity> {

    public TEntity withId(long id);
    public void add(TEntity entity);
    public void modify(TEntity entity);
    public void remove(TEntity entity);
}
